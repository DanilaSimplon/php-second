<?php

$arg_tab = [];
//iterate params without the first(file name)
foreach (array_slice($argv, 1) as $val) {
    //counts words nums
    if (str_word_count($val) === 1) {
        $arg_tab[] = $val;
    } else {
        //split the string & returns an array
        $new_val = explode(' ', $val);
        //iterate and push in the array
        foreach ($new_val as $new_value) {
            $arg_tab[] = $new_value;
        }
    }
}
//do the sorting
$res = sort($arg_tab);
//iterate and display
foreach ($arg_tab as $res) {
    echo $res;
    echo "\n";
}
