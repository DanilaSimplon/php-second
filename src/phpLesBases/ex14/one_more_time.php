<?php
//check if param
if (!empty($argv[1])) {
    //cut first param
    $str = array_slice($argv, 1);
    
    //check if '' or array
    if (gettype($str) == 'array') {
        //stringify array
        $str = implode(' ', $str);
       // $str = strtolower($str);    solve pb with CaseSensitive!!!!!!
    }
} else {
    exit;
}
//create array day/month...
$tab = explode(' ', $str);
//assign values
$day = $tab[0];

$month = $tab[2];
//////////////                  $month = strtolower($month);
//check day validity
if ($tab[1] > 0 && $tab[1] < 32) {
    $daynum = $tab[1];
} else {
    echo 'Wrong Format';
    echo "\n";
    exit;
}
//assign year
if (!empty($tab[3])) {
    $year = $tab[3];
}
//assign hour
if (!empty($tab[4])) {
    //translate into array
    $hour = explode(':', $tab[4]);
    $hou = $hour[0];
    $min = $hour[1];
    $sec = $hour[2];
    if ($hou > 23 || $min > 59 || $sec > 59) {
        echo 'Wrong Format';
        echo "\n";
        exit;
    }
}


$day_tab = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
$month_tab = [
    'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre', 'janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'
];

//check day validity
if (!in_array($day, $day_tab)) {
    echo 'Wrong Format';
    echo "\n";
    exit;
}
//check month validity
if (in_array($month, $month_tab)) {
    //look for month corresponding key
    $key = (array_search($month, $month_tab) + 1);
    if ($key > 12) {
        //Case Sensitive
        $key = ($key - 12);
    } else {
        $key = $key;
    }
} else {
    echo 'Wrong Format';
    echo "\n";
    exit;
}

//check date validity
if (checkdate($key, $daynum, $year)) {
    //add string to date format
    $str2 = $str . ' heure normale d’Europe centrale';

    $parser = new IntlDateFormatter(
        'fr_FR',
        IntlDateFormatter::FULL,
        IntlDateFormatter::FULL
    );
    $tsparis = $parser->parse($str2);
    //return int
    echo $tsparis;
    echo "\n";
}

