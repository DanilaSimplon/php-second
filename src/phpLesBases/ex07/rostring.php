<?php
//check if at least one param is given(argc counts params)
if ($argc < 2) {
    exit;
}
//get the first given param
$my_arg = $argv[1];
//Strip whitespace in the middle
$string_rep = preg_replace('/\s\s+/', ' ', $my_arg);
//Strip whitespace from the beginning and end 
$string_tr = trim($string_rep);
//counts words nums
$my_word_count = str_word_count($string_tr);
if ($my_word_count > 1) {
    //split the string & returns an array
    $my_new_tab = explode(' ', $string_tr);
    $num_first = $my_new_tab[0];
    //take out the first world
    unset($my_new_tab[0]);
    //push it back at the end of the array
    $my_new_tab[] = $num_first;
    //joint array into a string separated by ' '
    $my_last_word=implode(' ',$my_new_tab);
    echo $my_last_word;
    echo "\n";
}else {
    echo $string_tr;
    echo "\n";
}





