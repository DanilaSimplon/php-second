<?php
$file =$argv[1];
//get file content
$read = file_get_contents($file);
//get <a></a> content (link content bonus)
preg_match_all("/<a[^>]*>(.*)<\/a>/isU", $read, $matches);

//regex only <a></a> content (Davy)
//  /<a.*>.*<\/a>/
//'/>.*</'

//get links
foreach($matches[1] as $a_tag){
    $link= preg_replace('/<.*>/','',$a_tag);
    $links[]=$link;
}
//modify links to uppercase
foreach($links as $link){
    $upper = strtoupper($link);
    $read = str_replace($link,$upper,$read);
}
//get title string
foreach($matches[0] as $title_str ){
    preg_match_all('/(?:title=)"(.*)"/', $title_str, $matches3);
    $titles[]=$matches3[1][0];
}
//modify titles to uppercase
foreach($titles as $title){
    $upper_t = strtoupper($title);
    $read = str_replace($title,$upper_t,$read);
}

echo $read;