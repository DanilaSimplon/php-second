<?php
//check fields
if (empty($_POST['login']) || empty($_POST['passwd']) || ($_POST['submit'] != 'OK')) {
    echo "ERROR\n";

    return;
}

$file = './private/passwd';
//stock user in $login
$login = [
    'login' => $_POST['login'],
    'passwd' =>password_hash($_POST['passwd'],PASSWORD_BCRYPT)
    // hash('sha512', $_POST['passwd'], false),
];
//check if user already in file
if (file_exists($file)) {
    $users = file_get_contents($file);
    //userialize datas
    $users = unserialize($users);
    foreach ($users as $user) {
        if ($user['login'] == $_POST['login']) {
            echo "ERROR\n";

            return;
        }
    }
}
//put new user in a tab
$users[] = $login;
$serial = serialize($users);
//create new dir if doesn't exist
if (!file_exists($file)) {
    mkdir('./private', 0777);
}
//if pb while writing => err
if (!file_put_contents($file, $serial)) {
    echo "ERROR\n";

    return;
}
echo "OK\n";
