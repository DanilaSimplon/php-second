<?php
if ($argc < 3) {
    exit;
}
//stock first arg as key
$my_key = $argv[1];
//stock all the others args
$my_pairs = array_slice($argv, 2);
//iterate through the other args
foreach ($my_pairs as $my_pair) {
    //create new tab with key/pairs(:)
    $new_tab = explode(':', $my_pair);
    //put the key in $k
    $k = $new_tab[0];
    //put the value in $v and push it in $v_tab
    if (!empty($new_tab[1])) {
        $v = $new_tab[1];
        $v_tab[] = $v;
    }
    //check first key with other keys
    if ($k == $my_key) {
        if (isset($v)) {
            $v_tab[] = $v;
        }
    }
}
//display $v_tab last value
if (isset($v_tab)) {
    echo end($v_tab);
    echo "\n";
}
