<?php

//stock params without the first(file name)
$my_args = array_slice($argv, 1);
$my_list_tab = [];
foreach ($my_args as $my_arg) {
    //check if there's only one word inside the param
    if (preg_split('/\s+/', $my_arg, -1,         PREG_SPLIT_NO_EMPTY) == false) {
        $my_list_tab[] = $my_arg;
    } else {
        //split string & return array
        $my_list_tab2 = preg_split('/\s+/', $my_arg, -1,         PREG_SPLIT_NO_EMPTY);
        //iterate second array & add item to the first array
        foreach ($my_list_tab2 as $my_list) {
            $my_list_tab[] = $my_list;
        }
    }
}
//iterate first array & put each value in the right(alpha, number, or other) one
foreach ($my_list_tab as $my_value) {
    if (is_numeric($my_value)) {
        $num_tab[] = $my_value;
        //desc order for numbers
        sort($num_tab, SORT_STRING);///SORTIR DE LA BOUCLE LES SORT!!!!!!
    } elseif (ctype_alpha($my_value)) {
        $string_tab[] = $my_value;
        //order & case insensive
        natcasesort($string_tab);
    } else {
        $car_tab[] = $my_value;
        //order
        sort($car_tab);
    }
}
//display each item
foreach ($string_tab as $string) {
    echo $string;
    echo "\n";
}
foreach ($num_tab as $num) {
    echo $num;
    echo "\n";
}
//in case no special char
if (isset($car_tab)) {
    foreach ($car_tab as $car) {
        echo $car;
        echo "\n";
    }
}
