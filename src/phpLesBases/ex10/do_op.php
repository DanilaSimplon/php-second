<?php
//stock params without the first(file name)
$my_args = array_slice($argv, 1);
//check n° of params
if ($argc !== 4) {
    echo 'Incorrect Parameters';
    echo "\n";
} else {
    //check that first & third are numbers, and second a valid op
    if ((preg_match('!\d+!', $my_args[0], $n1)) && (preg_match('/\+|\*|\/|\-|\%/', $my_args[1], $op)) && (preg_match('!\d+!', $my_args[2], $n2))) {
        //extract nums & op
        preg_match('!\d+!', $my_args[0], $n1);
        preg_match('/\+|\*|\/|\-|\%/', $my_args[1], $op);
        preg_match('!\d+!', $my_args[2], $n2);
        //preg_match returns an array
        $n1 = $n1[0];
        $op = $op[0];
        $n2 = $n2[0];
        //do the ops
        if ($op == "+") {
            echo $n1 + $n2;
            echo "\n";
        } elseif ($op == "-") {
            echo $n1 - $n2;
            echo "\n";
        } elseif ($op == "*") {
            echo $n1 * $n2;
            echo "\n";
        } elseif ($op == "/") {
            if ($n2 != 0) {
                echo $n1 / $n2;
                echo "\n";
            } else {
                echo 0;
                echo "\n";
            }
        } elseif ($op == "%") {
            echo $n1 % $n2;
            echo "\n";
        }
    } else {
        //if the preg_match test fails
        echo 'Incorrect Parameters';
        echo "\n";
    }
}
