<?php

// while (true) {
//     echo "Entrez un nombre: ";
//     //iterate (forever/cntrlC)

//     //Strip whitespace from the end of a string
//     //Gets line from file pointer
//     //An already opened stream(STDN)
//     $input = rtrim(fgets(STDIN));
//     if (is_numeric($input)) {
//         //check if is an even(!=odd) num
//         if ($input % 2 == 0) {
//             echo "Le chiffre " . $input . " est Pair\n";
//         } else {
//             echo "Le chiffre " . $input . " est Impair\n";
//         }
//     } else {
//         echo "'" . $input . "' n'est pas un chiffre\n";
//     }
// }

echo 'Entrez un nombre: ';
while (true) {
    $chiffre = trim(fgets(STDIN));

    if (is_numeric($chiffre)) {
        if ($chiffre % 2 == 0) {
            echo 'Le chiffre ' . $chiffre . " est Pair\nEntrez un nombre: ";
        } else {
            echo 'Le chiffre ' . $chiffre . " est Impair\nEntrez un nombre: ";
        }
    } else {
        echo "'" . $chiffre . "' n'est pas un chiffre\nEntrez un nombre: ";
    }
}
